//
// Created by john on 2022. 03. 18..
//
#include <filesystem>
#include <iostream>
#include <fstream>
#include <chrono>
#include <vector>

#include "sqlite3.h"

#include "SlimSync/ChangesetWriter.hpp"
#include "SlimSync/ChangesetBuffer.hpp"
#include "SlimSync/Exception.hpp"


namespace fs = std::filesystem;

namespace SlimSync
{
    void ChangesetWriter::write(const ChangesetBuffer* changeset, const std::string& path, const std::string& fileExtension)
    {
        if (!directoryExists(path))
        {
            throw Exception("Path: " + path + " Must exist and be a directory!");
        }

        auto now = std::chrono::system_clock::now();
        auto timestamp = std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch()).count();
        std::string fileName =  std::to_string(timestamp) + "." + fileExtension;
        auto filePath = fs::path(path) / fileName;

        std::ofstream outStream;
        outStream.open(filePath, std::ios::out | std::ios::binary | std::ios::trunc);
        if (outStream.is_open())
        {
            outStream.write(reinterpret_cast<const char *>(changeset->getBlob()), changeset->getSize());
            outStream.close();
        }
        else
        {
            throw Exception("Could not open file: " + filePath.string());
        }

    }

    bool ChangesetWriter::directoryExists(const std::string &path) {

        return fs::exists(path) && fs::is_directory(path);
    }

    bool ChangesetWriter::collect(sqlite3* dbHandle, const std::string &changesetPath, const std::string &outputPath,
                                  const std::string &fileName) {

        if (!directoryExists(changesetPath))
        {
            throw Exception("Changeset Path: " + changesetPath + " Does not exist or is not a directory!");
        }

        if (!directoryExists(outputPath))
        {
            throw Exception("Output Path: " + outputPath + " Does not exist or is not a directory!");
        }

        sqlite3_changegroup *changeGroup;

        int ret = sqlite3changegroup_new(&changeGroup);
        if (ret != SQLITE_OK)
        {
            throw Exception(dbHandle, ret);
        }

        int changesetsAdded = 0;

        for (const auto& dir_entry : fs::directory_iterator(changesetPath))
        {
            if (!is_regular_file(dir_entry.path()))
                continue;

            auto size = file_size(dir_entry.path());
            std::ifstream inStream(dir_entry.path(), std::ifstream::binary);
            if (inStream.good())
            {
                std::vector<char*> buffer(size);
                inStream.read((char*)&buffer[0],(std::streamsize) size);
                ret = sqlite3changegroup_add(changeGroup, (int) buffer.size(), buffer.data());
                if (ret != SQLITE_OK)
                {
                    sqlite3changegroup_delete(changeGroup);
                    throw Exception(dbHandle, ret);
                }
                changesetsAdded += 1;
            }
        }

        //if no changesets were added don't continue
        if (changesetsAdded == 0)
        {
            sqlite3changegroup_delete(changeGroup);
            return false;
        }

        int size;
        void* changesetBlob;
        ret = sqlite3changegroup_output(changeGroup, &size, &changesetBlob);
        if (ret != SQLITE_OK)
        {
            sqlite3changegroup_delete(changeGroup);
            throw Exception(dbHandle, ret);
        }

        // now write buffer to file
        auto writePath = fs::path(outputPath) / fs::path(fileName);

        std::ofstream outStream;
        outStream.open(writePath, std::ios::out | std::ios::binary | std::ios::trunc);
        if (outStream.is_open())
        {
            outStream.write(reinterpret_cast<const char *>(changesetBlob), size);
            outStream.close();
        }
        else
        {
            sqlite3_free(changesetBlob);
            sqlite3changegroup_delete(changeGroup);
            throw Exception("Could not open file: " + writePath.string());
        }

        //free our blob and changegroup objects
        sqlite3_free(changesetBlob);
        sqlite3changegroup_delete(changeGroup);
        return true;
    }
}
