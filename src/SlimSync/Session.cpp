#include "SlimSync/Session.hpp"
#include "SlimSync/ChangesetBuffer.hpp"
#include "SlimSync/Exception.hpp"

extern "C" {
    #include "sqlite3.h"
}


namespace SlimSync
{

    Session::Session(sqlite3 *db, const std::string& databaseName)
    {
        const int ret = sqlite3session_create(db, databaseName.c_str(), &session);
        if (ret != SQLITE_OK)
        {
            throw Exception(db, ret);
        }
    }

    Session::~Session()
    {
        sqlite3session_delete(session);
    }

    void Session::attach(const std::string& tableName)
    {
        const int ret = sqlite3session_attach(session, tableName.c_str());
        if (ret != SQLITE_OK)
        {
            throw Exception("Could not attach table named: " + tableName);
        }
    }

    std::unique_ptr<SlimSync::ChangesetBuffer> Session::collectChangeset(sqlite3 *dbHandle)
    {
        int size;
        void* changesetBlob;

        const int ret = sqlite3session_changeset(session, &size, &changesetBlob);

        if (ret != SQLITE_OK)
        {
            throw Exception(dbHandle, ret);
        }

        auto changeset = std::make_unique<SlimSync::ChangesetBuffer>(size, changesetBlob);
        return changeset;
    }

}
