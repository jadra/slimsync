//
// Created by john on 2022. 04. 03..
//

#include "SlimSync/Exception.hpp"
#include "sqlite3.h"

namespace SlimSync
{
    Exception::Exception (const char* anErrorMessage, int retCode) :
            std::runtime_error(anErrorMessage),
            mErrcode(retCode),
            mExtendedErrcode(-1)
    {

    }

    Exception::Exception(sqlite3* pSQLite) :
            std::runtime_error(sqlite3_errmsg(pSQLite)),
            mErrcode(sqlite3_errcode(pSQLite)),
            mExtendedErrcode(sqlite3_extended_errcode(pSQLite))
    {
    }

    Exception::Exception(sqlite3* pSQLite, int ret) :
            std::runtime_error(sqlite3_errmsg(pSQLite)),
            mErrcode(ret),
            mExtendedErrcode(sqlite3_extended_errcode(pSQLite))
    {
    }

    // Return a string, solely based on the error code
    const char* Exception::getErrorStr() const noexcept
    {
        return sqlite3_errstr(mErrcode);
    }


}
