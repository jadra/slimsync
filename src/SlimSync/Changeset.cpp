//
// Created by john on 2022. 03. 30..
//

#include "SlimSync/Changeset.hpp"
#include "SlimSync/Exception.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include "sqlite3.h"


namespace fs = std::filesystem;

namespace SlimSync
{
    Changeset::Changeset(const std::filesystem::path& path) {
        mPath = path;
    }

    static int default_conflict_handler(void *pCtx, int eConflict, sqlite3_changeset_iter *pIter) {
        if (eConflict == SQLITE_CHANGESET_DATA || eConflict == SQLITE_CHANGESET_CONFLICT)
        {
            return SQLITE_CHANGESET_REPLACE;
        }

        if (eConflict == SQLITE_CHANGESET_NOTFOUND)
        {
            return SQLITE_CHANGESET_OMIT;
        }

        return SQLITE_CHANGESET_ABORT;
    }

    void Changeset::apply(sqlite3 *db, on_apply_conflict conflictHandler)
    {
        auto xConflict = conflictHandler == nullptr ? &default_conflict_handler : conflictHandler;

        for (const auto& dir_entry : fs::directory_iterator(mPath))
        {
            if (!is_regular_file(dir_entry.path()))
                continue;

            auto size = file_size(dir_entry.path());
            std::ifstream inStream(dir_entry.path(), std::ifstream::binary);
            if (inStream.good() && size > 0)
            {
                std::vector<char*> buffer(size);
                inStream.read((char*)&buffer[0],(std::streamsize) size);
                const int ret = sqlite3changeset_apply(db, (int)buffer.size(), buffer.data(),
                                       nullptr, xConflict, nullptr);
                if (ret == SQLITE_MISUSE)
                {
                    throw Exception(db, ret);
                }
            }
        }
    }
}
