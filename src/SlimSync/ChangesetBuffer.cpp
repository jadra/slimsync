#include <iostream>
#include "SlimSync/ChangesetBuffer.hpp"
extern "C" {
    #include "sqlite3.h"
}


namespace SlimSync
{
    ChangesetBuffer::ChangesetBuffer(int size, void* blob) {
        mSize = size;
        mBlob = blob;
        std::cout << "In ChangesetBuffer constructor\n";
    }

    ChangesetBuffer::~ChangesetBuffer()
    {
        std::cout << "In ChangesetBuffer destructor\n";
        sqlite3_free(mBlob);
    }

    int ChangesetBuffer::getSize() const
    {
        return mSize;
    }

    void* ChangesetBuffer::getBlob() const
    {
        return mBlob;
    }
}