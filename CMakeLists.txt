cmake_minimum_required(VERSION 3.21)
project(SlimSync)
include(FetchContent)

set(CMAKE_CXX_STANDARD 17)

#SQLite3 Dependency fetch

FetchContent_Declare(
        SQLite3
        GIT_REPOSITORY https://gitlab.com/jadra/sqlite3.git
        GIT_TAG 65fc19e511ce84e7ff501da10a4371d14abd7e45  # naster
)

FetchContent_MakeAvailable(SQLite3)


set(SLIMSYNC_SRC
        ${PROJECT_SOURCE_DIR}/src/SlimSync/Changeset.cpp
        ${PROJECT_SOURCE_DIR}/src/SlimSync/ChangesetBuffer.cpp
        ${PROJECT_SOURCE_DIR}/src/SlimSync/ChangesetWriter.cpp
        ${PROJECT_SOURCE_DIR}/src/SlimSync/Session.cpp
        ${PROJECT_SOURCE_DIR}/src/SlimSync/Exception.cpp)

# SlimSync
add_library(SlimSync STATIC ${SLIMSYNC_SRC})

target_include_directories(SlimSync PUBLIC ${PROJECT_SOURCE_DIR}/include)
target_link_libraries(SlimSync PUBLIC SQLite3)

# Tests

set(SLIMSYNC_TESTS_SRC
        ${PROJECT_SOURCE_DIR}/tests/changeset_tests.cpp
        ${PROJECT_SOURCE_DIR}/tests/changeset_writer_tests.cpp
        )


add_executable(tests tests/main.cpp ${SLIMSYNC_TESTS_SRC})

# copy our test changeset folder so we can reference them
add_custom_command(TARGET tests POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${PROJECT_SOURCE_DIR}/tests/changesets ${PROJECT_BINARY_DIR}/changesets
        COMMENT "Copied changesets ${PROJECT_BINARY_DIR}/changesets"
)

target_link_libraries(tests PUBLIC SlimSync)

# Install

include(GNUInstallDirs)

install(TARGETS SlimSync SQLite3
        EXPORT ${PROJECT_NAME}Targets
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
        COMPONENT Libraries)

install(DIRECTORY include/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
        COMPONENT headers FILES_MATCHING REGEX ".*\\.(hpp|h)$")

set(CPACK_PACKAGE_NAME "SlimSyncLib")
set(CPACK_PACKAGE_VENDOR "JD")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
        "SlimSyncLib - SlimSync Library Installer")
set(CPACK_PACKAGE_VERSION_MAJOR "0")
set(CPACK_PACKAGE_VERSION_MINOR "1")
set(CPACK_PACKAGE_VERSION_PATCH "0")
include(CPack)
