//
// Created by john on 2022. 04. 03..
//
#include <memory>
#include "doctest.h"
#include "sqlite3.h"
#include "SlimSync/SlimSync.hpp"


using namespace std;
using namespace SlimSync;
namespace fs = std::filesystem;


TEST_CASE("Changeset collection success") {
    sqlite3 *db;
    sqlite3_open(":memory:", &db);

    Session session(db);
    sqlite3_exec(db, "CREATE TABLE aTable(id INTEGER PRIMARY KEY, content TEXT)", nullptr, nullptr, nullptr);
    CHECK_NOTHROW(session.attach("aTable"));
    sqlite3_exec(db, "insert into aTable values (NULL, 'some text here')", nullptr, nullptr, nullptr);
    unique_ptr<ChangesetBuffer> changeset;
    CHECK_NOTHROW(changeset = session.collectChangeset(db));
    CHECK(changeset->getSize() > 0);
    CHECK(changeset->getBlob() != nullptr);
    sqlite3_close(db);
}

TEST_CASE("Changeset collection failure (Bad Table Name)") {
    sqlite3 *db;
    sqlite3_open(":memory:", &db);

    Session session(db);
    sqlite3_exec(db, "CREATE TABLE aTable(id INTEGER PRIMARY KEY, content TEXT)", nullptr, nullptr, nullptr);
    CHECK_NOTHROW(session.attach("BadTableName"));
    sqlite3_exec(db, "insert into aTable values (NULL, 'some text here')", nullptr, nullptr, nullptr);
    unique_ptr<ChangesetBuffer> changeset;
    CHECK_NOTHROW(changeset = session.collectChangeset(db));
    CHECK(changeset->getSize() == 0);
    CHECK(changeset->getBlob() == nullptr);
    sqlite3_close(db);
}



TEST_CASE("Changeset write failure") {
    sqlite3 *db;
    sqlite3_open(":memory:", &db);

    Session session(db);
    sqlite3_exec(db, "CREATE TABLE aTable(id INTEGER PRIMARY KEY, content TEXT)", nullptr, nullptr, nullptr);
    session.attach("aTable");
    sqlite3_exec(db, "insert into aTable values (NULL, 'some text here')", nullptr, nullptr, nullptr);
    unique_ptr<ChangesetBuffer> changeset = session.collectChangeset(db);
    CHECK_THROWS(ChangesetWriter::write(changeset.get(), "nosuchdir"));
    sqlite3_close(db);
}