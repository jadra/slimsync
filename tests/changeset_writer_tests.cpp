//
// Created by john on 2022. 04. 03..
//
#include <memory>
#include <filesystem>
#include "sqlite3.h"
#include "doctest.h"
#include "SlimSync/SlimSync.hpp"

namespace fs = std::filesystem;
using namespace std;
using namespace SlimSync;

TEST_CASE("Changeset write success") {
    auto outputPath = fs::path("output");
    fs::remove_all(outputPath);
    fs::create_directory(outputPath);
    sqlite3 *db;
    sqlite3_open(":memory:", &db);
    Session session(db);
    sqlite3_exec(db, "CREATE TABLE aTable(id INTEGER PRIMARY KEY, content TEXT)", nullptr, nullptr, nullptr);
    session.attach("aTable");
    sqlite3_exec(db, "insert into aTable values (NULL, 'some text here')", nullptr, nullptr, nullptr);
    unique_ptr<ChangesetBuffer> changeset = session.collectChangeset(db);
    CHECK_NOTHROW(ChangesetWriter::write(changeset.get(), "output"));

    int count = 0;
    for (const auto& dir_entry : fs::directory_iterator(outputPath))
    {
        count++;
    }
    CHECK(count == 1);
    fs::remove_all("output");
    sqlite3_close(db);
}

TEST_CASE("Changeset collection success") {
    auto changesetPath = fs::path("changesets");
    auto collectedPath = fs::path("collected");
    auto latestChangesetFile = fs::path("latest.changeset");

    fs::remove_all(collectedPath);
    fs::create_directory(collectedPath);
    sqlite3 *db;
    sqlite3_open(":memory:", &db);

    bool ret = ChangesetWriter::collect(db,
                                        changesetPath.string(),
                                        collectedPath.string(),
                                        latestChangesetFile.string());
    CHECK(ret);
    CHECK(fs::exists(collectedPath / latestChangesetFile));
    fs::remove_all("collected");
    sqlite3_close(db);
}

TEST_CASE("Changest application success") {

    auto changesetPath = fs::path("changesets");
    auto collectedPath = fs::path("collected");
    auto latestChangesetFile = fs::path("latest.changeset");
    fs::remove_all(collectedPath);
    fs::create_directory(collectedPath);

    {
        fs::remove_all(collectedPath);
        fs::create_directory(collectedPath);

        sqlite3 *db;
        sqlite3_open(":memory:", &db);

        ChangesetWriter::collect(db,
                                            changesetPath.string(),
                                            collectedPath.string(),
                                            latestChangesetFile.string());
        sqlite3_close(db);
    }

    {
        sqlite3 *db;
        sqlite3_open(":memory:", &db);

        sqlite3_exec(db, "CREATE TABLE aTable(id INTEGER PRIMARY KEY, content TEXT)", nullptr, nullptr, nullptr);
        Changeset changeset(collectedPath);
        changeset.apply(db);

        sqlite3_stmt *stmt;
        sqlite3_prepare_v2(db, "select count(id) as ct from aTable", -1, &stmt, 0);
        sqlite3_step(stmt);

        int count = sqlite3_column_int(stmt, 0);
        CHECK(count == 900);

        sqlite3_finalize(stmt);
        sqlite3_close(db);
    }

    fs::remove_all("collected");
}




