//
// Created by john on 2022. 04. 03..
//

#ifndef SLIMSYNC_EXCEPTION_HPP
#define SLIMSYNC_EXCEPTION_HPP

#include <stdexcept>
#include <string>

struct sqlite3;

namespace SlimSync
{
    class Exception : public std::runtime_error
    {
    public:
        Exception(const char* anErrorMessage, int retCode);

        Exception(const std::string& anErrorMessage, int retCode) :
                Exception(anErrorMessage.c_str(), retCode)
        {

        }

        explicit Exception(const char* aErrorMessage) :
                Exception(aErrorMessage, -1) //retCode < 0 to not class with SQLITE_OK
        {
        }

        explicit Exception(const std::string& aErrorMessage) :
                Exception(aErrorMessage.c_str(), -1) //retCode < 0 to not class with SQLITE_OK
        {
        }

        explicit Exception(sqlite3* pSQLite);

        Exception(sqlite3* pSQLite, int ret);

        /// Return the result code (if any, otherwise -1).
        int getErrorCode() const noexcept
        {
            return mErrcode;
        }

        /// Return the extended numeric result code (if any, otherwise -1).
        int getExtendedErrorCode() const noexcept
        {
            return mExtendedErrcode;
        }

        /// Return a string, solely based on the error code
        const char* getErrorStr() const noexcept;

    private:
        int mErrcode;         ///< Error code value
        int mExtendedErrcode; ///< Detailed error code if any
    };
}

#endif //SLIMSYNC_EXCEPTION_HPP
