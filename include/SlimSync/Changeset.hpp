//
// Created by john on 2022. 03. 30..
//

#ifndef SLIMSYNC_CHANGESET_HPP
#define SLIMSYNC_CHANGESET_HPP

#include <filesystem>

struct sqlite3;
struct sqlite3_changeset_iter;


namespace SlimSync
{
    typedef int (*on_apply_conflict)(void *, int, sqlite3_changeset_iter *);

    class Changeset {
    public:
        explicit Changeset(const std::filesystem::path& path);
        ~Changeset() = default;
        void apply(sqlite3* db, on_apply_conflict conflictHandler = nullptr);
    private:
        std::filesystem::path mPath;
    };
}


#endif //SLIMSYNC_CHANGESET_HPP
