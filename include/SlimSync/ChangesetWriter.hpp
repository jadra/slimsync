//
// Created by john on 2022. 03. 18..
//

#ifndef SLIMSYNC_CHANGESETWRITER_HPP
#define SLIMSYNC_CHANGESETWRITER_HPP

#include <string>

struct sqlite3;

namespace SlimSync
{
    class ChangesetBuffer;

    class ChangesetWriter
    {
    public:
         ChangesetWriter() = default;
        ~ChangesetWriter() = default;

        static void write(const ChangesetBuffer* changeset, const std::string& path,
                          const std::string& fileExtension = "changeset");

        static bool collect(sqlite3* dbHandle, const std::string& changesetPath, const std::string& outputPath,
                            const std::string& fileName);

    private:
        static bool directoryExists(const std::string &path);
    };
}


#endif //SLIMSYNC_CHANGESETWRITER_HPP
