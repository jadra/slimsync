#pragma once


namespace SlimSync
{
    class ChangesetBuffer
    {
        public:
            ChangesetBuffer(int size, void *buffer);
            ~ChangesetBuffer();
            
            //Can't copy changeset
            ChangesetBuffer(const ChangesetBuffer&) = delete;
            ChangesetBuffer& operator=(const ChangesetBuffer&) = delete;

            //Can move changeset
            ChangesetBuffer(ChangesetBuffer&& aChangeset) = default;
            ChangesetBuffer& operator=(ChangesetBuffer&& aChangeset) = default;

            int   getSize() const;
            void* getBlob() const;

        private:
            int mSize;
            void* mBlob;
    };
}