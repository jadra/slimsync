#pragma once
#include <string>
#include <memory>

// Forward declaration to avoid including sqlite3.h in header.
struct sqlite3;
struct sqlite3_session;

namespace SlimSync
{
    //ChangesetBuffer forward declaration
    class ChangesetBuffer;

    class Session
    {
    public:
        explicit Session(sqlite3 *db, const std::string& databaseName = "main");

        //Can't copy session
        Session(const Session&) = delete;
        Session& operator=(const Session&) = delete;

        //Can move session
        Session(Session&& aSession) = default;
        Session& operator=(Session&& aSession) = default;

        ~Session();

        void attach(const std::string& tableName);

        std::unique_ptr<SlimSync::ChangesetBuffer> collectChangeset(sqlite3 *dbHandle);
    
    private:
        sqlite3_session *session{};
    };

}